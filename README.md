# Тестовое задание для Citek

## Быстрый старт

    ./gradlew

## Разработка

Перед тем как начинать

    yarn install

Устанавливаем гальп

    yarn global add gulp-cli

## Соборка для продакшена

    ./gradlew -Pprod clean bootRepackage

Для чека что все работает

    java -jar build/libs/*.war

Переходим [http://localhost:8080](http://localhost:8080) в браузере

## Тестирование


### Фронтенд

    gulp test

### Стресс тест

    ./gradlew gatlingRun

## Докер и профиль -Pprod

    docker-compose -f src/main/docker/postgresql.yml up -d

Для остановки постгреса

    docker-compose -f src/main/docker/postgresql.yml down

Билд докер образа 

    ./gradlew bootRepackage -Pprod buildDocker

Контейнерезация

    docker-compose -f src/main/docker/app.yml up -d

