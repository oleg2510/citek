(function () {
    'use strict';

    angular
        .module('joggingApp')
        .controller('ElasticsearchReindexController', ElasticsearchReindexController);

    ElasticsearchReindexController.$inject = [];

    function ElasticsearchReindexController() {
    }
})();
