(function() {
    'use strict';

    angular
        .module('joggingApp')
        .controller('JoggingDialogController', JoggingDialogController);

    JoggingDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Jogging', 'User','Principal','$http'];

    function JoggingDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Jogging, User,Principal,$http) {
        var vm = this;

        vm.jogging = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.user = null;

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.user = User.get({login:account.login});
            });
        }

        getAccount();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            vm.jogging.userId = vm.user.id;
            if (vm.jogging.id !== null) {
                Jogging.update(vm.jogging, onSaveSuccess, onSaveError);
            } else {
                Jogging.save(vm.jogging, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('joggingApp:joggingUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
            $http.post("/api/elasticsearch/index").then(function (data) {});
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.finish = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
