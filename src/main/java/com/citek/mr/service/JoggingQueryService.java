package com.citek.mr.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.citek.mr.domain.Jogging;
import com.citek.mr.domain.*; // for static metamodels
import com.citek.mr.repository.JoggingRepository;
import com.citek.mr.repository.search.JoggingSearchRepository;
import com.citek.mr.service.dto.JoggingCriteria;

import com.citek.mr.service.dto.JoggingDTO;
import com.citek.mr.service.mapper.JoggingMapper;

/**
 * Service for executing complex queries for Jogging entities in the database.
 * The main input is a {@link JoggingCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link JoggingDTO} or a {@link Page} of {%link JoggingDTO} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class JoggingQueryService extends QueryService<Jogging> {

    private final Logger log = LoggerFactory.getLogger(JoggingQueryService.class);


    private final JoggingRepository joggingRepository;

    private final JoggingMapper joggingMapper;

    private final JoggingSearchRepository joggingSearchRepository;

    public JoggingQueryService(JoggingRepository joggingRepository, JoggingMapper joggingMapper, JoggingSearchRepository joggingSearchRepository) {
        this.joggingRepository = joggingRepository;
        this.joggingMapper = joggingMapper;
        this.joggingSearchRepository = joggingSearchRepository;
    }

    /**
     * Return a {@link List} of {%link JoggingDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<JoggingDTO> findByCriteria(JoggingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Jogging> specification = createSpecification(criteria);
        return joggingMapper.toDto(joggingRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {%link JoggingDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<JoggingDTO> findByCriteria(JoggingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Jogging> specification = createSpecification(criteria);
        final Page<Jogging> result = joggingRepository.findAll(specification, page);
        return result.map(joggingMapper::toDto);
    }

    /**
     * Function to convert JoggingCriteria to a {@link Specifications}
     */
    private Specifications<Jogging> createSpecification(JoggingCriteria criteria) {
        Specifications<Jogging> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Jogging_.id));
            }
            if (criteria.getFinish() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFinish(), Jogging_.finish));
            }
            if (criteria.getTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTitle(), Jogging_.title));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getUserId(), Jogging_.user, User_.id));
            }
        }
        return specification;
    }

}
