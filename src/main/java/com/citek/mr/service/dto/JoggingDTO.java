package com.citek.mr.service.dto;


import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Jogging entity.
 */
public class JoggingDTO implements Serializable {

    private Long id;

    private ZonedDateTime finish;

    @NotNull
    @Size(min = 3)
    private String title;

    private Long userId;

    private String userLogin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getFinish() {
        return finish;
    }

    public void setFinish(ZonedDateTime finish) {
        this.finish = finish;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        JoggingDTO joggingDTO = (JoggingDTO) o;
        if(joggingDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), joggingDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "JoggingDTO{" +
            "id=" + getId() +
            ", finish='" + getFinish() + "'" +
            ", title='" + getTitle() + "'" +
            "}";
    }
}
